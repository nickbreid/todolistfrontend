import Route from '@ember/routing/route';

export default Route.extend({
  model() {
    return this.store.findAll('task');
  },

  actions: {
    toggleDone: function(taskId) {
      this.store.findRecord('task', taskId,  { backgroundReload: false }).then(task => {
        task.toggleProperty('isDone');
        task.save();
      })
    },
    removeTask: function(taskId) {
      this.store.findRecord('task', taskId,  { backgroundReload: false }).then(task => {
        task.destroyRecord();
      })
    },
    editTask: function(taskId, elementId) {
      // outermost ember div
      let taskToBeEdited = document.getElementById(elementId);
      // list item inside ember div
      let listItemForThisTask = taskToBeEdited.getElementsByClassName('incomplete')[0];
      // span inside list item
      let elementWithTaskTitle = taskToBeEdited.getElementsByClassName('incomplete-task-text')[0];
      // text inside span
      let taskTitle = elementWithTaskTitle.innerHTML;

      // function to save and display updated task title
      let saveThisTask = (newTaskTitle) => this.store.findRecord('task', taskId,  { backgroundReload: false }).then(task => {
        task.set('title', newTaskTitle);
        task.save();
        formForNewTitle.remove();
        listItemForThisTask.appendChild(elementWithTaskTitle);
        taskTitle = newTaskTitle;
      });

      // when the user clicks a task title, we'll replace that text
      // with a form/input for the updated task title
      // on submit, it'll pass the new title to saveThisTask()
      let formForNewTitle = document.createElement("form");
      formForNewTitle.setAttribute('class', 'form-group');
      formForNewTitle.setAttribute('type', 'text');
      formForNewTitle.style.cssText = 'display: inline;';
      formForNewTitle.addEventListener("submit", function(event) {
        event.preventDefault();
        let newTaskTitle = formForNewTitle.childNodes[0].value;
        saveThisTask(newTaskTitle);
      });
      formForNewTitle.addEventListener("blur", function(event) {
        event.preventDefault();
        let newTaskTitle = formForNewTitle.childNodes[0].value;
        saveThisTask(newTaskTitle);
      }, true);

      // inside the form goes a text input pre-filled with the existing title
      let inputForNewTitle = document.createElement("input");
      inputForNewTitle.setAttribute('class', 'form-control');
      inputForNewTitle.type = "text";
      inputForNewTitle.value = taskTitle;

      // so with all that settled, the editTask action
      // removes the element with existin TaskTitle text
      elementWithTaskTitle.remove();
      // and appends a new input with an onSubmit action of saveThisTask()
      listItemForThisTask.appendChild(formForNewTitle).appendChild(inputForNewTitle).focus();
    }
  }
});
