import Component from '@ember/component';
import { select } from 'd3-selection';
import { axisBottom, axisLeft } from 'd3-axis';
import { scaleLinear, scaleBand} from 'd3-scale';
import { format } from 'd3-format';

export default Component.extend({
  doneCount: Ember.computed('tasks.@each.isDone', function() {
    let tasks = this.tasks;
    return tasks.filterBy('isDone', true).length;
  }),
  todoCount: Ember.computed('tasks.@each.isDone', function() {
    let tasks = this.tasks;
    return tasks.filterBy('isDone', false).length;
  }),
  doneCountChanged: Ember.observer('doneCount', function(){
    console.log("doneCount changed!");
    let body = document.getElementsByClassName('chart-container')[0];
    let svg = select(document.getElementById('chart'));
    svg.remove();

    let newSvg =  document.createElementNS("http://www.w3.org/2000/svg", "svg");
    newSvg.setAttribute('id', 'chart');
    body.appendChild(newSvg);

    this.didInsertElement();
  }),
  todoCountChanged: Ember.observer('todoCount', function(){
    let body = document.getElementsByClassName('chart-container')[0];
    let svg = select(document.getElementById('chart'));
    svg.remove();

    let newSvg =  document.createElementNS("http://www.w3.org/2000/svg", "svg");
    newSvg.setAttribute('id', 'chart');
    body.appendChild(newSvg);

    this.didInsertElement();
  }),
  didInsertElement() {
    let svg = select(document.getElementById('chart')),
      height = 300,
      width = 350,
      margin = {top: 40, right: 20, bottom: 70, left: 40},
      tasks = [
        { status: "Done", count: this.doneCount },
        { status: "Todo", count: this.todoCount }
      ],
      taskCounts = tasks.map(t => t.count),
      taskTitles = tasks.map(t => t.status)

    let yScale = scaleLinear()
      .domain([ 0, Math.max(...taskCounts) ])
      .range([ height, 0 ]);

    let xScale = scaleBand()
      .domain(taskTitles)
      .range([ 0, width ])
      .paddingInner(0.12);

    let xAxis = axisBottom(xScale);
    let gX = svg.append('g')
    .attr("class", "axis-text")
    .attr("transform", `translate(${margin.left},${height+9})`, margin.left, height)
    .call(xAxis);

    let yAxis = axisLeft(yScale)
                .tickValues([0, ...taskCounts])
                .tickFormat(format('.0f'));
    let gY = svg.append('g')
    .attr("class", "axis-text")
    .attr("transform", `translate(${margin.left - 5},4)`, margin.left)
    .call(yAxis);

    svg.selectAll('rect')
    .data(tasks)
    .enter()
    .append('rect')
    .attr('width', xScale.bandwidth())
    .attr('height', task => yScale(0) - yScale(task.count) )
    .attr('x', task => xScale(task.status)  + margin.left )
    .attr('y', task => yScale(task.count) + 4 )
    .style("opacity", .6)
    .attr('fill', task => task.status === 'Done' ? '#0078D7' : '#A9A9A9');

  }
});
