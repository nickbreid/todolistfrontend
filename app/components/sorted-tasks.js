import Component from '@ember/component';

export default Component.extend({
  classNames: ['all-tasks'],
  orderBy: ['isDone:asc', 'numericID:desc'],
  sortedTasks: Ember.computed.sort('tasks', 'orderBy'),
  doneCount: Ember.computed('tasks.@each.isDone', function() {
    let tasks = this.tasks;
    return tasks.filterBy('isDone', true).length;
  })
});
