import DS from 'ember-data';

export default DS.Model.extend({
  title: DS.attr(),
  isDone: DS.attr(),
  numericID: Ember.computed('id', function() { return Number(this.get('id')) })
});
