import Controller from '@ember/controller';

export default Controller.extend({
  actions: {
    postTask() {
      let that = this;
      let title = this.get('title');
      let newRecord = { title: title, isDone: 0};
      this.store.createRecord('task', newRecord).save()
      .then(function() {
        that.set('title', '');
      });
    },
    hideCompleted() {
      let completeTasks = document.getElementsByClassName('complete-tasks')[0];
      let icon = document.getElementById('complete-label-icon').innerHTML;

      if (icon == '▼') {
        icon = '►'
      } else if (icon == '►') {
        icon = '▼'
      }

      document.getElementById('complete-label-icon').innerHTML = icon;
      completeTasks.classList.toggle('hidden');
    }
  }
});
